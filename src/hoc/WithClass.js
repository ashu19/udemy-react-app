import React from 'react';

const withClass = (WrappedComponenet, className) => {
    return props => (
        <div className={className}>
            <WrappedComponenet {...props}></WrappedComponenet>
        </div>
    );
};

export default withClass;