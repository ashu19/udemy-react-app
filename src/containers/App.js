import React, { Component } from 'react';
import myClasses from './App.css';
import Persons from '../componenets/Persons/Persons';
import Cockpit from '../componenets/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Auxi from '../hoc/Auxi';
import AuthContext from '../context/auth-context';

class App extends Component {
  constructor(props) {
    super(props);
    console.log('[App.js] Consructor');
  }

  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] getDerivedStateFromProps', props);
    return state;
  }

  // componentWillMount() {
  //   console.log('[App.js] componentWillMount');
  // }

  componentDidMount() {
    console.log('[App.js] componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[App.js] shouldComponentUpdate');
    return true;
  }

  componentDidUpdate() {
    console.log('[App.js] componentDidUpdate');
  }

  state = {
    persons: [
      { id: 'aa', name: 'Max', age: 26 },
      { id: 'bb', name: 'Lopu', age: 27 },
      { id: 'cc', name: 'Ashu', age: 28 }
    ],
    otherState: 'some other value',
    showPersons: false,
    showCockpit: true,
    changeCounter: 1,
    authentcated: false
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState((prevState, props) => {
      return {
        persons: persons,
        changeCounter: prevState.changeCounter + 1
      }
    });
  }

  loginHandler = () => {
    this.setState({ authentcated: true });
  }

  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({ persons: persons });
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({ showPersons: !doesShow });
  }

  render() {
    console.log('[App.js] render');

    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <Persons
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.nameChangedHandler}
          isAuthenticated={this.state.authentcated}>
        </Persons>
      );
    }

    return (
      <Auxi>
        <button onClick={() => {
          this.setState({ showCockpit: !this.state.showCockpit })
        }}>Toggle cockpit</button>
        <AuthContext.Provider value={{
          authenticated: this.state.authentcated,
          login: this.loginHandler
        }}>
          {this.state.showCockpit ? <Cockpit
            title={this.props.appTitle}
            showPersons={this.state.showPersons}
            personsLength={this.state.persons.length}
            clicked={this.togglePersonsHandler}>
          </Cockpit> : null}
          {persons}
        </AuthContext.Provider>
      </Auxi>
    );

    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Hi I am a react app!!!'));
  }
}

export default withClass(App, myClasses.App);
