import React, { useEffect, useRef, useContext } from 'react';
import myClasses from './Cockpit.css';
import AuthContext from '../../context/auth-context';

const cockpit = (props) => {
    const toggleButtonRef = useRef(null);
    const authContext = useContext(AuthContext);

    useEffect(() => {
        console.log('[Cockpit.js] useEffect');

        // const timer = setTimeout(() => {
        //     alert('Saved data');
        // }, 1000);
        toggleButtonRef.current.click();
        return () => {
            console.log('[Cockpit.js] Cleanup work in effect');
        }
    }, []);

    useEffect(() => {
        console.log('[Cockpit.js] 2 useEffect');
        return () => {
            console.log('[Cockpit.js] Cleanup work in 2nd effect');
        }
    });

    let classes = [];
    let btnClasses = '';

    if (props.showPersons) {
        btnClasses = myClasses.Red;
    }

    if (props.personsLength <= 2) {
        classes.push(myClasses.red);
    }

    if (props.personsLength <= 1) {
        classes.push(myClasses.bold);
    }

    return (
        <div className={myClasses.Cockpit}>
            <h1>{props.title}</h1>
            <p className={classes.join(' ')}>This is working!!!!!!</p>
            <button ref={toggleButtonRef} className={btnClasses} onClick={props.clicked}>
                Toggle Persons
            </button>
            <button onClick={authContext.login}>Log in</button>
        </div>
    );
}

export default React.memo(cockpit);